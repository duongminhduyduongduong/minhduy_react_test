import Screen from "./components/Screen";
import { sortLargest } from "./functions";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import Confirm from "./components/Confirm";

function App() {
    const arr: number[] = [54, 546, 60, 548];

    return (
        <Router>
            <div className="App">
                <div>
                    <h1 className="text-orange-500">Bài 1</h1>
                    <div>{sortLargest(arr)}</div>
                </div>
            </div>
            <Routes>
                <Route path="/" element={<Screen />} />
                <Route path="/confirm" element={<Confirm />} />
            </Routes>
        </Router>
    );
}

export default App;
