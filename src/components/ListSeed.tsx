import React, { useEffect, useState } from "react";
import { createItemList } from "../functions";
import { IFinalResult, IItem, IResult } from "../typing";

interface IHeaderProps {
    text: string;
}

const ListSeed: React.FC<IHeaderProps> = ({ text }) => {
    const [listItems, setListItems] = useState<IFinalResult[]>([]);
    const [itemSelected, setItemSelected] = useState("");
    const [itemActive, setItemActive] = useState("");

    useEffect(() => {
        const items = JSON.parse(localStorage.getItem("items") as string);
        if (items) {
            setListItems(items);
        }
    }, []);
    console.log(listItems);

    const onChangeSelected = (seed: string) => {
        setItemSelected(seed);
    };

    return (
        <div className="px-2 mb-6">
            <p className="text-md text-blue1 font-medium mb-[16px] flex justify-between">{text} <span className="text-black">2/6</span></p>
            <div className="">
                {listItems?.map((item, index) => (
                    <div>
                        <div className="border border-neutral4 rounded-md py-1 px-4 flex justify-between items-center mb-[16px]">
                            <div
                                key={index}
                                className="w-[50px] h-[50px] rounded-full flex justify-center items-center border border-blue1 text-blue1 font-medium"
                            >
                                {item.primary}
                            </div>
                            <div className="flex justify-between items-center">
                                {item.list?.map((seed, index) => (
                                    <div
                                        key={index}
                                        className={`py-[6px] px-[12px] cursor-pointer ${
                                            seed === itemSelected
                                                ? "bg-blue3 text-blue1 rounded-md"
                                                : ""
                                        }`}
                                        onClick={() => onChangeSelected(seed)}
                                    >
                                        {seed}
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default ListSeed;
