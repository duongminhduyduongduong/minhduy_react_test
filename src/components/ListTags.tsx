import React, { useEffect, useState } from "react";
import { createItemList } from "../functions";
import { IItem, IResult } from "../typing";

interface IHeaderProps {
    text: string;
}

const ListTags: React.FC<IHeaderProps> = ({ text }) => {
    const [tags, setTags] = useState([""]);
    const [listItems, setListItems] = useState<IItem[]>([]);

    useEffect(() => {
        getAllItemsAPI();
    }, []);

    const getAllItemsAPI = async () => {
        try {
            fetch("https://metanode.co/json/eng.json")
                .then((res) => res.json())
                .then((result) => {
                    setTags(result);
                });
        } catch (error) {
            console.log(error);
        }
    };

    useEffect(() => {
        const result = createItemList(tags, 24);
        const listSeed = result.result;
        setListItems(result.items as IItem[]);
        localStorage.setItem("items", JSON.stringify(listSeed));
    }, [tags]);

    return (
        <div className="px-2">
            <p className="text-md text-blue1 font-medium mb-[16px]">{text}</p>
            <div className="grid grid-cols-3 gap-1.5">
                {listItems?.map((item, index) => (
                    <div className="col-span-1 flex items-center gap-[8px] bg-white py-[6px] px-[8px] rounded-md">
                        <div className="w-2 h-2 bg-blue3 rounded-full flex justify-center items-center text-blue1 text-sm">
                            {index + 1}
                        </div>
                        <div>{item.name}</div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default ListTags;
