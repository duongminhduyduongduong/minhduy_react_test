import React from 'react'
import Bottom from './Bottom'
import Copy from './Copy'
import Header from './Header'
import ListTags from './ListTags'

function Screen() {
  return (
    <div className='mt-2 w-[375px] bg-background pt-6'>
        <Header />
        <ListTags text='Auto Gen Seed Phrase?' />
        <Copy />
        <Bottom text='NEXT' />
    </div>
  )
}

export default Screen