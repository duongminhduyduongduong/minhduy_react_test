import React from "react";
import Bottom from "./Bottom";
import Copy from "./Copy";
import Header from "./Header";
import ListSeed from "./ListSeed";
import ListTags from "./ListTags";

function Confirm() {
    return (
        <div className="mt-2 w-[375px] bg-background pt-6">
            <Header />
            <ListSeed text="Confirm Your Seed Phrase" />
            <Bottom text="SUBMIT" />
        </div>
    );
}

export default Confirm;
