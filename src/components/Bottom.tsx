import React from "react";
import { Link } from "react-router-dom";

import Button from "./Button";

interface IBottomProps {
    text: string;
}

const Bottom: React.FC<IBottomProps> = ({ text }) => {
    return (
        <div className="bg-white pt-[27px] pb-[36px] px-2">
            <div className="flex justify-between items-center">
                <div className="text-neutral text-md font-medium">
                    How does this work?
                </div>
                <svg
                    width="30"
                    height="30"
                    viewBox="0 0 30 30"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <path
                        d="M12.5 10L17.5 15L12.5 20"
                        stroke="#04004D"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                    />
                </svg>
            </div>
            <Link to={"/confirm"}>
                <Button text={text} />
            </Link>
        </div>
    );
};

export default Bottom;
