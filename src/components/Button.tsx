import React from "react";

interface IButtonProps {
    text: string;
}

const Button: React.FC<IButtonProps> = ({ text }) => {
    return (
        <button className="bg-blue1 flex items-center justify-center text-white text-md font-bold rounded-lg py-2 mt-3 w-full">
            {text}
        </button>
    );
};

export default Button;
