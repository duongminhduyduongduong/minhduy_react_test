export interface IItem {
    name: string;
    index: number;
}
export interface IResult {
    items?: IItem[];
    stringArr?: string[];
    result?: IFinalResult[];
}
export interface IFinalResult {
    list?: string[];
    primary?: number;
}
