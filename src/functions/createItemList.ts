import { IFinalResult, IItem, IResult } from "../typing";

const maxLength = 6;

export const checkExist = (arr: string[], word: string) => {
    if (arr.includes(word)) return true;

    return false;
};

export const createItemList = (arr: string[], amount: number): IResult => {
    const draftStringList: string[] = [];
    const draftItemList: IItem[] = [];

    for (let i = 0; i < arr.length; i++) {
        if (draftItemList.length < amount) {
            const arrItem = arr[Math.floor(Math.random() * arr.length)];

            if (!checkExist(draftStringList, arrItem)) {
                draftStringList.push(arrItem);
                draftItemList.push({
                    name: arrItem,
                    index: arr.indexOf(arrItem),
                });
            }
        } else {
            break;
        }
    }

    const result: IFinalResult[] = [];
    const resultString: string[] = [];

    for (let i = 0; i < amount; i++) {
        if (result.length < maxLength) {
            const draftArr: IItem[] = [];

            for (let i = 0; i < 3; i++) {
                const arrItem =
                    draftItemList[
                        Math.floor(Math.random() * draftItemList.length)
                    ];
                    
                if (!checkExist(resultString, arrItem.name)) {
                    draftArr.push(arrItem);
                }
            }
            result.push({
                list: draftArr.map((item) => item.name),
                primary: draftArr[0].index,
            });
        }
    }

    return { items: draftItemList, stringArr: draftStringList, result };
};
