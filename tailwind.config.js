/** @type {import('tailwindcss').Config} */

const colors = {
    blue1: "#004DFF",
    blue3: "#BBCFFB",
    neutral: "#04004D",
    neutral4: "#D3D7DB",
    background: "#F7F7F7",
    white: "#FFFFFF",
    black: "#000000"
};

const fontSize = {
    DEFAULT: "15px",
    sm: "11px",
    md: "17px"
};

const fontWeight = {
  DEFAULT: 400,
  normal: 400,
  medium: 500,
  bold: 700
};

const renderSpacings = () => {
    const spacing = {
        full: "100%",
        fit: "fit-content",
    };
    // 0px --> 100px | 0px 5px 10px 15px,...
    for (let i = 0; i < 10; i += 0.5) {
        spacing[i] = `${i * 10}px`;
    }
    // 100px --> 1000px | 100px 110px 120px,...
    for (let i = 10; i < 100; i++) {
        spacing[i] = `${i * 10}px`;
    }
    return spacing;
};

const spacing = renderSpacings();

const borderRadius = {
    md: "6px", // medium,
    lg: "10px"
};
module.exports = {
    content: ["./src/**/*.{js,jsx,ts,tsx}"],
    theme: {
        colors,
        fontSize,
        fontWeight,
        spacing,
        extend: {
            borderRadius,
        },
    },
    plugins: [],
};
